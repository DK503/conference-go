import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state + ",1",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    resp = requests.get(url, params=params)
    locale = json.loads(resp.content)
    lat, lon = locale[0]["lat"], locale[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY, "units": "imperial"}
    resp = requests.get(url, params=params)
    locale = json.loads(resp.content)
    try:
        return {
            "temp": str(locale["main"]["temp"]) + "°F",
            "description": locale["weather"][0]["description"],
        }
    except:
        return None
